#include <EventBookkeeperTools/BookkeeperDumperTool.h>

#include "../AllExecutedEventsCounterAlg.h"
#include "../BookkeeperTool.h"
#include "../CutFlowSvc.h"
#include "../EventCounterAlg.h"
#include "../SkimDecisionMultiFilter.h"
#include "../StreamSelectorTool.h"
#include "../TestFilterReentrantAlg.h"

DECLARE_COMPONENT( AllExecutedEventsCounterAlg )
DECLARE_COMPONENT( BookkeeperDumperTool )
DECLARE_COMPONENT( BookkeeperTool )
DECLARE_COMPONENT( CutFlowSvc )
DECLARE_COMPONENT( EventCounterAlg )
DECLARE_COMPONENT( SkimDecisionMultiFilter )
DECLARE_COMPONENT( StreamSelectorTool )
DECLARE_COMPONENT( TestFilterReentrantAlg )
